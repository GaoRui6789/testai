#!/usr/bin/python
# encoding:utf-8
# import argparse
# import click
# import paddle
import waveletai
# from paddle.vision.transforms import Compose,Normalize
# import LeNetModel


# def train(opt,train_dataset):
#     epochs,batch_size = opt.epochs,opt.batch_size
#     #定义一个预处理流程
#     #ToTensor() 操作会将像素值除以 255，将它们标准化到 0-1 范围内。
#     #需要注意的是，data_format="CHW"是标准的格式，代表的是Channel, Height, Width，即通道数，高度和宽度。
#     #开始训练
#     #调用定义好的LeNet模型
#     model = paddle.Model(LeNetModel.LeNetModel())
#     #定义优化器、损失函数以及评价标准
#     #使用Adam优化器，二值交叉熵损失、使用精度作为评价标准
#     model.prepare(paddle.optimizer.Adam(parameters=model.parameters()),
#                   paddle.nn.CrossEntropyLoss(),paddle.metric.Accuracy())
#     model.fit(train_dataset,epochs=epochs,batch_size=batch_size,verbose=1)
#     model.save("./model1")
#     return model



# def val(model,val_dataset):
#     acc = model.evaluate(val_dataset, verbose=1)
#     return acc["acc"]
# def parse_opt():
#     parser = argparse.ArgumentParser()
#     parser.add_argument('--epochs', type=int, default=1, help='ep')
#     parser.add_argument('--batch_size',type=int,default=32,help='total batch size')
#     return parser.parse_args()

@click.command()
@click.option("--epochs", '-ep', type=int, default=1)
@click.option("--batch_size", '-bs', type=int, default=64)
def enter(epochs,batch_size):
    print(epochs,batch_size)
    # waveletai.init(api_token="eyJhcGlfdXJsIjogImh0dHA6Ly8xNzIuMTguNjguNTAvYXBpIiwgInVzZXJfaWQiOiAiZWQ2ODNjYWQxYmFlNDQ0Y2FhYmFmNGY4YTg1MzljMzkiLCAiYXBpX3Rva2VuIjogImY3ZWQ1ZjAzZWFlMmNmZTdkZjFhZGNlZTQ5NmE2ZWVhZDk4YzVmZTQyYTcyZDczOTc3N2NhOGQ4N2NkMGI3YjYifQ==")
    # waveletai.set_app(app_id="db1aa4d0b068497ba7ffb4bca474143d")
    #数据集
    # transform = Compose([Normalize(mean=[127.5],std=[127.5],data_format="SHW")])
    # #导入MNIST数据
    # train_dataset = paddle.vision.datasets.MNIST(mode="train",transform=transform)
    # val_dataset = paddle.vision.datasets.MNIST(mode="test", transform=transform)
    # opt = parse_opt()
    # opt.batch_size = batch_size
    # opt.epochs = epochs
    # waveletai.log_param("batch_size_value", batch_size)
    # waveletai.log_param("epochs_value", epochs)
    # model = train(opt,train_dataset)
    # acc = val(model,val_dataset)
    # waveletai.log_metric("acc", acc)

if __name__ == "__main__":
    enter()
