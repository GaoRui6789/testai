import paddle
import paddle.nn.functional as F
class LeNetModel(paddle.nn.Layer):
    def __init__(self):
        super(LeNetModel,self).__init__()
        #卷积层1
        self.conv1 = paddle.nn.Conv2D(in_channels=1,
                                      out_channels=6,
                                      kernel_size=5,
                                      stride=1)
        #池化层1
        self.pool1 = paddle.nn.MaxPool2D(kernel_size=2,
                                         stride=2)
        #卷积层2
        self.conv2 = paddle.nn.Conv2D(in_channels=6,
                                      out_channels=16,
                                      kernel_size=5,
                                      stride=1)
        #池化层2
        self.pool2 = paddle.nn.MaxPool2D(kernel_size=2,
                                         stride=2)
        #线性层1
        self.fc1 = paddle.nn.Linear(256,120)
        #线性层2
        self.fc2 = paddle.nn.Linear(120,84)
        #线性层3
        self.fc3 = paddle.nn.Linear(84,10)
    #正向传播过程
    def forward(self, x):
        x = self.conv1(x)
        x = F.sigmoid(x)
        x = self.pool1(x)
        x = self.conv2(x)
        x = F.sigmoid(x)
        x = self.pool2(x)
        #flatten 用于将张量展平
        x = paddle.flatten(x,start_axis=1,stop_axis=-1)
        x = self.fc1(x)
        x = self.fc2(x)
        out = self.fc3(x)
        return out


if __name__ == "__main__":

    pass